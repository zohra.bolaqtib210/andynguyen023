# Andy Nguyen

AndyNguyen - [Andy Nguyen](https://andynguyen.com.vn) Channel :

Kinh Nghiệm Và Thành Tích Nổi Bật Của Andy Nguyen:

– Hiện đang là Giám đốc Phát triển Kinh doanh tại ERA Vietnam.

– 21 năm kinh nghiệm đa dạng trong các lĩnh vực B2C, B2B và BDS và cũng là nhà đầu tư bất động sản và chứng khoán.

– Nhiều năm kinh nghiệm làm tại Shell – tập đoàn dầu khí và năng lượng lớn trên thế giới với vai trò là Giám đốc toàn quốc mảng B2B.

 #andynguyen #andychannel #andyera #andynguyenera

https://lotus.vn/w/blog/andy-nguyen-andy-nguyen-era-kinh-doanh-thuc-chien-607298290102304768.htm

https://www.behance.net/andynguyen023/info

https://www.flickr.com/people/198892066@N06/
